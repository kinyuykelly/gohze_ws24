// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
   devtools: { enabled: true },

   // Global CSS
   css: ["@/assets/scss/style.scss"],

   public: {
      directory: ["fonts"], // Add the 'fonts' directory to the public directory
   },

   vite: {
      css: {
         preprocessorOptions: {
            scss: {
               additionalData: `@import '@/assets/scss/variables.scss';`,
            },
         },
      },
      assetsInclude: ["**/*.html"],
   },

   runtimeConfig: {
      public: {
        gtagId: 'G-1EHXVW5SGB',
      }
    },

   // Plugins to load before mounting the app
   plugins: [{ src: "@/plugins/bootstrap.js", mode: "client" }],

   modules: [
      "@nuxt/image",
      "@nuxtjs/i18n",
      "vue3-carousel-nuxt",
      "@vee-validate/nuxt",
      "@vueuse/nuxt",
      "nuxt-aos",
   ],

   buildModules: ["@nuxtjs/composition-api/module"],

   build: {
      rollupOptions: {
         external: [
            // Other external modules...
            "photoswipe/lightbox",
         ],
      },
   },

   app: {
      head: {
         title: "GOHZE",
         // titleTemplate: "",
         meta: [
            { charset: "utf-8" },
            {
               name: "viewport",
               content: "width=device-width, initial-scale=1",
            },
            {
               hid: "description",
               name: "description",
               content:
                  "GOHZE Group est une entreprise de services numérique spécialisée dans le développement des projets innovants à fort apport technologique et social",
            },
         ],
         link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
      },
      head: {
         title: "GOHZE",
         meta: [
            { charset: "utf-8" },
            {
               name: "viewport",
               content: "width=device-width, initial-scale=1",
            },
            {
               hid: "description",
               name: "description",
               content:
                  "GOHZE Group est une entreprise de services numérique spécialisée dans le développement des projets innovants à fort apport technologique et social",
            },
         ],
         link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
      },
   },

   i18n: {
      lazy: true,
      langDir: "locales",
      strategy: "prefix_except_default",
      locales: [
         {
            code: "en",
            iso: "en-US",
            name: "English(US)",
            file: "en.json",
         },
         {
            code: "fr",
            iso: "fr-FR",
            name: "French(FR)",
            file: "fr.json",
         },
      ],
      defaultLocale: "fr",
      vueI18n: "./nuxt-i18n.js",
      baseUrl: "https://gohze.com",
      htmlAttrs: {
         lang: "{{ $i18n.locale }}",
      },
      compilation: {
         strictMessage: false,
         // escapeHtml: true,
      },
   },

   carousel: {
      prefix: "MyPrefix",
   },

   aos: {
      // Global settings:
      disable: false, // accepts following values: 'phone', 'tablet', 'mobile', boolean, expression or function
      startEvent: "DOMContentLoaded", // name of the event dispatched on the document, that AOS should initialize on
      initClassName: "aos-init", // class applied after initialization
      animatedClassName: "aos-animate", // class applied on animation
      useClassNames: false, // if true, will add content of `data-aos` as classes on scroll
      disableMutationObserver: false, // disables automatic mutations' detections (advanced)
      debounceDelay: 50, // the delay on debounce used while resizing window (advanced)
      throttleDelay: 99, // the delay on throttle used while scrolling the page (advanced)

      // Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
      offset: 30, // offset (in px) from the original trigger point
      delay: 0, // values from 0 to 3000, with step 50ms
      duration: 1000, // values from 0 to 3000, with step 50ms
      easing: "ease-in-sine", // default easing for AOS animations
      once: true, // whether animation should happen only once - while scrolling down
      mirror: false, // whether elements should animate out while scrolling past them
      anchorPlacement: "top-bottom", // defines which position of the element regarding to window should trigger the animation
   },
});
