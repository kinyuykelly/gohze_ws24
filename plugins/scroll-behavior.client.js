import { defineNuxtPlugin } from "#app";
export default defineNuxtPlugin(({ $router }) => {
   $router.options.scrollBehavior = function (to, from, savedPosition) {
      if (to.hash) {
         return {
            selector: to.hash,
            top: 30,
            behavior: "smooth",
         };
      } else if (savedPosition) {
         return savedPosition;
      } else {
         return { x: 0, y: 0 };
      }
   };
});
